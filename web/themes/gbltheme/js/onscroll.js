/*global Drupal */
(function ($, Drupal) {

  'use strict';

  /**
   * On scroll effects.
   */
  Drupal.behaviors.onscroll = {
    attach: function (context, settings) {
      
      // Check if an element is scrolled into view.
      $.fn.isScrolledIntoView = function (element, fullyInView) {
        var pageTop = $(window).scrollTop();
        var pageBottom = pageTop + $(window).height();
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).height();

        if (fullyInView === true) {
          return ((pageTop < elementTop) && (pageBottom > elementBottom));
        }
        else {
          return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
        }
      };

      // Loop through different scroll divs and add class when visible.
      $.fn.loopThroughScrolls = function () {
        // When div has to be fully visible.
        $('.js-onscroll-full').each(function () {
          if ($.fn.isScrolledIntoView(this, true) === true) {
            $(this).addClass('is-visible')
          }
        });
        // When only a part of the div should be visible.
        $('.js-onscroll-part').each(function () {
          if ($.fn.isScrolledIntoView(this, false) === true) {
            $(this).addClass('is-visible')
          }
        });
      };

      // Move divs at different speed.
      $.fn.moveIt = function () {
        var $window = $(window);
        var instances = [];

        $(this).each(function () {
          instances.push(new moveItItem($(this)));
        });

        window.onscroll = function () {
          var scrollTop = $window.scrollTop() - $window.height();

          // Get position of element/
          instances.forEach(function (inst) {
            inst.update(scrollTop);
          });
        }
      };
      var moveItItem = function (el, speed) {
        this.el = $(el);
        this.speed = parseInt(this.el.attr('data-scroll-speed'));
      };
      moveItItem.prototype.update = function(scrollTop){
        // The offset changes according to banner height (100vh).
        var difference = (this.el.offset().top - $(window).height()) - scrollTop;
        this.el.css('transform', 'translateY(' + (difference / this.speed) + 'px)');
      };

      // Check if elements are in view, before we start scrolling.
      $.fn.loopThroughScrolls();

      // Define scroll speed for specific elements.
      $('[data-scroll-speed]').moveIt();

      $(window).scroll(function () {
        $.fn.loopThroughScrolls();
      });


    }
  };

})(jQuery, Drupal);