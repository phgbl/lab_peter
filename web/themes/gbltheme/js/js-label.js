/*global Drupal */
(function ($, Drupal) {

  'use strict';

  /**
   * Add extra class on form field when placeholder value is not visible.
   */
  Drupal.behaviors.formLabels = {
    attach: function (context, settings) {

      $(document).ready(function () {

        $('.site .form-type-textfield, .site .form-type-email, .site .form-type-tel').each(function (index) {
          var input = $(this).find('input');
          if (input.length === 0) {
            // No input found, check for textarea.
            input = $(this).find('textarea');
          }
          var $formitem = $(this);
            $formitem.addClass('js-label');
            $formitem.addClass('js-hide-label');
          if ($(input).val() !== '') {
            // If input field is not empty, show label.
              $formitem
              .removeClass('js-hide-label')
              .addClass('js-unhighlight-label');
          }
          $(input).on('keyup blur focus', function (e) {
            // Add or remove classes to show labels.
              if (e.type === 'keyup') {
                  if ($(input).val() === '') {
                      $(formitem).addClass('js-hide-label');
                  }
                  else {
                      $(formitem)
                        .removeClass('js-hide-label')
                        .addClass('js-unhighlight-label');
                  }
              }
              else {
                  if (e.type === 'blur') {
                      if ($(input).val() === '') {
                          $(formitem).addClass('js-hide-label');
                      }
                      else {
                          $(formitem)
                            .removeClass('js-hide-label')
                            .addClass('js-unhighlight-label');
                      }
                  }
                  else {
                      if (e.type === 'focus') {
                          if ($(input).val() !== '') {
                              $(formitem).removeClass('js-unhighlight-label');
                          }
                      }
                  }
              }
          });

        });

      });
    }
  };


})(jQuery, Drupal);