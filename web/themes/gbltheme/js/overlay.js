/*global Drupal */
(function ($, Drupal) {

    'use strict';

    /**
     * Mobile navigation.
     */
    Drupal.behaviors.menumobile = {
        attach: function (context, settings) {
            $('.js-overlay-toggler').once().click(function (e) {
                e.preventDefault();
                $('body').toggleClass('overlay-open');
            });
        }
    };

})(jQuery, Drupal);