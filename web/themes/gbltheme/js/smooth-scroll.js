/*global Drupal */
(function ($, Drupal) {

    'use strict';

    /**
     * Smoothscroll anchor navigation
     */
    Drupal.behaviors.smoothScroll = {
        attach: function (context, settings) {
            // When clicking on link with anchor. Animate the scroll.
            $('a[href*="#"]:not([href="#"])').once().click(function () {
                if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 500);
                        return false;
                    }
                }
            });
        }
    };

})(jQuery, Drupal);