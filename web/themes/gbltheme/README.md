nvm use 7.4.0

GBL theme
===========
GBL theme is a light Sass based Drupal starter theme.

First time using GBL theme
==========================
Install globally once.

- Install NodeJS globally

install and download node js: http://nodejs.org/


- Install Gulp globally (Javascript task runner)

sudo npm install --global gulp-cli


Install dependencies
====================
Install needed dependencies for every project.

- Go to root of gbltheme and install dependencies

npm install


Gulp tasks
===========
- gulp


Folder structure (Based on SMACSS, with a GBL twist)
===========
- Base:

Base rules are the defaults. They are almost exclusively single element selectors but it could include attribute selectors, pseudo-class selectors, child selectors or sibling selectors. Essentially, a base style says that wherever this element is on the page.

- Layout:

Divide the page into sections. Layouts hold one or more modules together. We also use layout for the items that appear on every page (e.g. header, footer, navigation,...).

- Module:

Are the reusable, modular parts of our design. They are the nodes, paragraphs, blocks,...

- Helpers:

Mixins, extendables,..