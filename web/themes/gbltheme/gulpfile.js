var autoprefix = require("gulp-autoprefixer"),
    gulp       = require("gulp"),
    bourbon    = require("bourbon").includePaths,
    neat       = require("bourbon-neat").includePaths,
    sass       = require("gulp-sass"),
    sassGlob   = require('gulp-sass-glob'),
    cleanCSS   = require('gulp-clean-css');

// Base scss paths.
var paths = {
    scss_src: ["./scss/*.scss"],
    scss_watch: ["./scss/**/*.scss"]
};

gulp.task("sass", function () {
    return gulp.src(paths.scss_src)
        .pipe(sassGlob())
        .pipe(sass({
            sourcemaps: true,
            includePaths: [bourbon, neat]
        }))
        .pipe(autoprefix("last 10 versions"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest("./css"))
});

gulp.task("default", ["sass"], function() {
    gulp.watch(paths.scss_watch, ["sass"]);
});