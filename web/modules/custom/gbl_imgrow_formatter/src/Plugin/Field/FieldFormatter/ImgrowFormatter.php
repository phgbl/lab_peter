<?php

/**
 * @file
 * Contains \Drupal\image_title_caption\Plugin\Field\FieldFormatter\RealizationFormatter.
 */

namespace Drupal\gbl_imgrow_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'Image row' formatter.
 *
 * @FieldFormatter(
 *   id = "imagerow",
 *   label = @Translation("Image row"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */

class ImgrowFormatter extends ImageFormatter {

  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    $entity = $items[0]->getEntity();

    $num = $entity->field_images_row->value;
    $style = 'imgrow_' . $num;

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $image) {
      $elements[$delta] = array(
        '#theme' => 'image_style',
        '#style_name' => $style,
        '#uri' => $image->getFileUri(),
      );
    }
    return $elements;
  }
}